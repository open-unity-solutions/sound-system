## [0.2.0] – 2023-08-01

### Fixed
- Requirements not applied to prefabs
- CI/CD updated to modern version

### Removed
- `KeyAsset` dependency removed

### Added
- `SoundReferenceTrigger` can be used with AssetReferences (Addressables)


## [0.1.1] - 2023-06-16

### Changes
- KeyAsset dependency updated
- License year updated

### Fixed
- CI/CD reference fixed


## [0.1.0] - 2023-02-14

### Changes
- Project exported
## [0.1.1] - 2023-06-16
### Changes
- KeyAsset dependency updated
- License year updated

### Fixed
- CI/CD reference fixed


## [0.1.0] - 2023-02-14

### Changes