﻿using System;
using UnityEngine;

#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif


namespace Valhalla.SoundSystem.Data
{
	public abstract partial class Sound<TChannel> : ScriptableObject
		where TChannel : Enum
	{
		[SerializeField]
		#if ODIN_INSPECTOR
		[AssetsOnly, Required]
		[PreviewField(200, ObjectFieldAlignment.Left)]
		#endif
		private AudioClip _audioClip;


		public abstract TChannel Channel { get; }


		public AudioClip AudioClip => _audioClip;

	}
}
