﻿#if SGP_ASYNC
using ScriptGraphPro.Attributes.Fields;
#endif


namespace Valhalla.SoundSystem
{
	public class TimerSettings
	{
		#if SGP_ASYNC
		[NodeContent("Time To Wait")]
		#endif
		public int TimeToWait = 10000;


		#if SGP_ASYNC
		[NodeContent("Timer")]
		#endif
		public bool IsTimed;


		public TimerSettings()
		{
		}


		public TimerSettings(int timeToWait = 10000, bool isTimed = false)
		{
			TimeToWait = timeToWait;
			IsTimed = isTimed;
		}
	}
}
