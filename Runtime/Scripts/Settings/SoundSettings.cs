﻿#if SGP_ASYNC
using ScriptGraphPro.Attributes.Fields;
#endif


namespace Valhalla.SoundSystem
{
	public class SoundSettings
	{
		#if SGP_ASYNC
		[NodeContent("Cycled")]
		#endif
		public bool IsCycled = true;


		#if SGP_ASYNC
		[NodeContent("Pitch")]
		#endif
		public float Pitch = 1;


		#if SGP_ASYNC
		[NodeContent("Volume")]
		#endif
		public float Volume = 1;


		public SoundSettings()
		{
		}


		public SoundSettings(float pitch = 1, float volume = 1, bool isCycled = true)
		{
			Pitch = pitch;
			Volume = volume;
			IsCycled = isCycled;
		}
	}
}
