﻿#if SGP_ASYNC
using ScriptGraphPro.Attributes.Fields;
#endif


namespace Valhalla.SoundSystem
{
	public class FadeSettings
	{
		
		#if SGP_ASYNC
		[NodeContent("Is fade enabled?")]
		#endif
		public bool IsFadeEnabled = true;
		
		
		#if SGP_ASYNC
		[NodeContent("FadeIn Time")]
		#endif
		public int FadeInTime = 1000;


		#if SGP_ASYNC
		[NodeContent("FadeOut Time")]
		#endif
		public int FadeOutTime = 1000;




		public FadeSettings()
		{
		}


		public FadeSettings(int fadeInTime = 1000, int fadeOutTime = 1000, bool isFadeEnabled = true)
		{
			FadeInTime = fadeInTime;
			FadeOutTime = fadeOutTime;
			IsFadeEnabled = isFadeEnabled;
		}
	}
}
