﻿#if SGP_ASYNC
using ScriptGraphPro.Attributes.Fields;
#endif

namespace Valhalla.SoundSystem
{
	public class StereoSettings
	{
		#if SGP_ASYNC
		[NodeContent("Fade")]
		#endif
		public bool IsSmoothly = true;
		
		
		#if SGP_ASYNC
		[NodeContent("Fade Time")]
		#endif
		public int FadeTime = 1000;


		#if SGP_ASYNC
		[NodeContent("New stereo value")]
		#endif
		public float NewStereoPanValue = 0;


		public StereoSettings()
		{
		}


		public StereoSettings(float newStereoPanValue, bool isSmoothly = false, int fadeTime = 1000)
		{
			NewStereoPanValue = newStereoPanValue;
			IsSmoothly = isSmoothly;
			FadeTime = fadeTime;
		}
	}
}
