﻿using System;
using UnityEngine;
using Valhalla.SoundSystem.Data;
using Sirenix.OdinInspector;


namespace Valhalla.SoundSystem
{
	public abstract class SoundTrigger<TSound, TChannel> : BaseSoundTrigger
		where TSound : Sound<TChannel>
		where TChannel : Enum
	{
		[SerializeField, AssetsOnly, Required]
		protected TSound _sound;
	}
}
