using UnityEngine;


namespace Valhalla.SoundSystem
{
	public abstract class BaseSoundTrigger : MonoBehaviour
	{
		protected abstract void Trigger();
	
		
		public void _OnSoundTrigger()
			=> Trigger();
	}
}
