using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;


namespace Valhalla.SoundSystem
{
	public abstract class SoundReferenceTrigger<TAssetReference, TAsset, TChannel> : BaseSoundTrigger
		where TAssetReference : AssetReferenceT<TAsset>
		where TAsset : ScriptableObject
		where TChannel : Enum
	{
		[SerializeField, Required]
		protected TAssetReference _soundReference;
	}
}
