﻿using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;

#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif


namespace Valhalla.SoundSystem
{
	public class AudioSourceShellSound : AudioSourceShell
	{
		[SerializeField]
		#if ODIN_INSPECTOR
		[ChildGameObjectsOnly, RequiredIn(PrefabKind.InstanceInScene)]
		#endif
		private List<AudioSource> _effectSources;

		private float _unglobalizedVolume;


		public override float GlobalVolume
			=> _settingsWrapper.GlobalSoundVolume;


		private float GlobalizedVolume => _unglobalizedVolume * GlobalVolume;
		
		public void UpdateGlobalVolume()
			=> _effectSources.ForEach(effectSource => effectSource.volume = GlobalizedVolume);

		private AudioSource AvailableEffectSource
		{
			get
			{
				foreach (var effectSource in _effectSources)
					if (!effectSource.isPlaying)
						return effectSource;
				
				var newEffectSourceGameObject = new GameObject("EffectSource" + (_effectSources.Count + 1));
				newEffectSourceGameObject.transform.parent = transform;
				var newEffectSource = newEffectSourceGameObject.AddComponent<AudioSource>();
				_effectSources.Add(newEffectSource);

				return newEffectSource;
			}
		}


		private void ApplySettingsForAudioSource(AudioSource audioSource, AudioClip sound, float volume, float pitch)
		{
			audioSource.volume = volume;
			audioSource.pitch = pitch;
			audioSource.PlayOneShot(sound);
		}
		
		public void PlaySound(AudioClip sound, float volume, float pitch)
		{
			_unglobalizedVolume = volume;

			ApplySettingsForAudioSource(AvailableEffectSource, sound, GlobalizedVolume, pitch);
		}


		private void ApplyStereoEffect(AudioSource audioSource, float initialStereoPan, StereoSettings stereoSettings)
		{
			audioSource.panStereo = initialStereoPan;
			audioSource.FadeStereoPan(CancellationToken.None, stereoSettings).Forget();
		}


		public void PlayStereoSound(AudioClip sound, float volume, float pitch, float initialStereoPan, StereoSettings stereoSettings)
		{
			_unglobalizedVolume = volume;

			var effectSource = AvailableEffectSource;
			ApplySettingsForAudioSource(effectSource, sound, GlobalizedVolume, pitch);
			ApplyStereoEffect(effectSource, initialStereoPan, stereoSettings);
		}
	}
}
