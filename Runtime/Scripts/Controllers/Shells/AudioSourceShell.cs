﻿using System.Threading;
using UnityEngine;

#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif 


namespace Valhalla.SoundSystem
{
	public abstract class AudioSourceShell : MonoBehaviour
	{
		[SerializeField]
		#if ODIN_INSPECTOR
		[SceneObjectsOnly, RequiredIn(PrefabKind.InstanceInScene)]
		#endif
		protected SoundSettingsWrapper _settingsWrapper;
		
		
		public abstract float GlobalVolume { get; }

		protected CancellationTokenSource _cancellationTokenSourceFadingStereoPan = new();
	}
}
