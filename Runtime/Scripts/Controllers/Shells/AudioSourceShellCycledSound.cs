﻿using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;

#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif


namespace Valhalla.SoundSystem
{
	public class AudioSourceShellCycledSound : AudioSourceShell
	{
		[SerializeField]
		#if ODIN_INSPECTOR
		[ChildGameObjectsOnly, Required]
		#endif
		private AudioSource _audioSourceA;


		[SerializeField]
		#if ODIN_INSPECTOR
		[ChildGameObjectsOnly, Required]
		#endif
		private AudioSource _audioSourceB;

		
		#if ODIN_INSPECTOR
		[ShowInInspector, ReadOnly]
		#endif
		private AudioClip _currentSound;


		private CancellationTokenSource _cancelTokenSourceMusic = new();
		
		
		private AudioSource CurrentAudioSource => 
			_currentSound == null ? null : 
			_audioSourceA.clip == _currentSound ? _audioSourceA : 
			_audioSourceB.clip == _currentSound ? _audioSourceB : null;
		
		private SoundSettings _unglobalizedSoundSettings;
		
		public override float GlobalVolume 
			=> _settingsWrapper.GlobalMusicVolume;


		private SoundSettings GlobalizedSoundSettings => new()
		{
			IsCycled = _unglobalizedSoundSettings.IsCycled,
			Pitch = _unglobalizedSoundSettings.Pitch,
			Volume = _unglobalizedSoundSettings.Volume * GlobalVolume,
		};
		
		
		private void Start()
		{
			StopImmediate();
		}


		public void UpdateGlobalVolume()
		{
			if(_currentSound != null)
				CurrentAudioSource.SetSettings(GlobalizedSoundSettings);
		}


		public async UniTask ChangeVolumeFading(SoundSettings soundSettings, int fadeTime)
		{
			if (_currentSound is null)
				return;
			
			_cancelTokenSourceMusic.Cancel();
			_cancelTokenSourceMusic = new CancellationTokenSource();
			var cancelToken = _cancelTokenSourceMusic.Token;

			_unglobalizedSoundSettings = soundSettings;

			var audioSourceCurrent = CurrentAudioSource;

			if (audioSourceCurrent == null)
				return;
			
			var audioSourceFading = audioSourceCurrent.Equals(_audioSourceA) ? _audioSourceB : _audioSourceA;

			await CrossFade(cancelToken, audioSourceFading, audioSourceCurrent, _currentSound, GlobalizedSoundSettings, new FadeSettings(fadeTime, fadeTime));
		}


		public void ChangeVolumeImmediate(SoundSettings soundSettings)
		{
			_unglobalizedSoundSettings = soundSettings;
			CurrentAudioSource.SetSettings(GlobalizedSoundSettings);
		}


		public async UniTask PlayFading(AudioClip sound, SoundSettings soundSettings, FadeSettings fadeSettings)
		{
			_cancelTokenSourceMusic.Cancel();
			_cancelTokenSourceMusic = new CancellationTokenSource();
			var cancelToken = _cancelTokenSourceMusic.Token;

			_unglobalizedSoundSettings = soundSettings;

			if (sound is null)
				Debug.LogError($"Fade-play null sound clip! Use StopFading instead or check sound assets!");
			
			var audioSourceCurrent = CurrentAudioSource;

			if (audioSourceCurrent != null && sound == _currentSound)
			{
				var audioSourceFading = audioSourceCurrent == _audioSourceA ? _audioSourceB : _audioSourceA;

				await CrossFade(cancelToken, audioSourceFading, audioSourceCurrent, sound, GlobalizedSoundSettings, fadeSettings);
				
				return;
			}

			if (_audioSourceA == audioSourceCurrent)
			{
				await CrossFade(cancelToken, _audioSourceA, _audioSourceB, sound, GlobalizedSoundSettings, fadeSettings);

				return;
			}

			if (_audioSourceB == audioSourceCurrent)
			{
				await CrossFade(cancelToken, _audioSourceB, _audioSourceA, sound, GlobalizedSoundSettings, fadeSettings);

				return;
			}

			await StartPlaySmoothly(cancelToken, _audioSourceA, sound, GlobalizedSoundSettings, fadeSettings.FadeInTime);
		}


		public async UniTask StopFading(int fadeOutTime = 1000)
		{
			_cancelTokenSourceMusic.Cancel();
			_currentSound = null;

			if (fadeOutTime == 0)
			{
				StopImmediate();

				return;
			}

			_cancelTokenSourceMusic = new CancellationTokenSource();
			var cancelToken = _cancelTokenSourceMusic.Token;

			var fadingTasks = new List<UniTask>();

			if (_audioSourceA.isPlaying)
				fadingTasks.Add(_audioSourceA.FadeOutAsync(cancelToken, fadeOutTime));

			if (_audioSourceB.isPlaying)
				fadingTasks.Add(_audioSourceB.FadeOutAsync(cancelToken, fadeOutTime));

			await UniTask.WhenAll(fadingTasks);
		}


		private async UniTask StartPlaySmoothly(CancellationToken token, AudioSource audioSource, AudioClip sound, SoundSettings soundSettings, int fadeInTime = 1000)
		{
			_currentSound = sound;
			
			audioSource.clip = sound;
			audioSource.SetSettings(soundSettings);

			if (fadeInTime > 0)
				await audioSource.FadeInAsync(token, soundSettings.Volume, fadeInTime);
			else
			{
				audioSource.clip = sound;
				audioSource.SetSettings(soundSettings);
				audioSource.Play();
			}
		}


		private async UniTask CrossFade(CancellationToken cancelToken, AudioSource audioSourceFadeOut, AudioSource audioSourceFadeIn, AudioClip sound, SoundSettings soundSettings, FadeSettings fadeSettings)
		{
			_currentSound = sound;
			
			var crossfadeTasks = new List<UniTask>();

			if (fadeSettings.FadeOutTime > 0)
				crossfadeTasks.Add(audioSourceFadeOut.FadeOutAsync(cancelToken, fadeSettings.FadeOutTime));
			else
			{
				audioSourceFadeOut.Stop();
				audioSourceFadeOut.volume = 0;
			}

			if(audioSourceFadeIn.clip == null || !audioSourceFadeIn.clip.Equals(sound))
				crossfadeTasks.Add(StartPlaySmoothly(cancelToken, audioSourceFadeIn, sound, soundSettings, fadeSettings.FadeInTime));
			else
				crossfadeTasks.Add(audioSourceFadeIn.FadeChangeVolume(cancelToken, soundSettings.Volume, fadeSettings.FadeInTime));
				

			await UniTask.WhenAll(crossfadeTasks);
		}


		public async UniTask FadeStereoPan(StereoSettings stereoSettings)
		{
			var currentAudioSource = CurrentAudioSource;

			if (currentAudioSource == null)
			{
				Debug.LogError("CurrentAudioSource is null!");
				return;
			}

			_cancellationTokenSourceFadingStereoPan.Cancel();
			_cancellationTokenSourceFadingStereoPan = new CancellationTokenSource();
			var cancelToken = _cancellationTokenSourceFadingStereoPan.Token;

			await currentAudioSource.FadeStereoPan(cancelToken, stereoSettings);
		}


		public void PlayImmediate(AudioClip sound, SoundSettings soundSettings)
		{
			StopImmediate();
			
			_currentSound = sound;

			_unglobalizedSoundSettings = soundSettings;

			_audioSourceA.clip = sound;
			_audioSourceA.SetSettings(GlobalizedSoundSettings);
			_audioSourceA.Play();
		}


		public void StopImmediate()
		{
			_currentSound = null;
			_audioSourceA.Stop();
			_audioSourceA.volume = 0;
			_audioSourceB.Stop();
			_audioSourceB.volume = 0;
		}
	}
}
