﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;

#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif


namespace Valhalla.SoundSystem
{
	[ExecuteAlways]
	public class SoundController : MonoBehaviour
	{
		[SerializeField]
		#if ODIN_INSPECTOR
		[ChildGameObjectsOnly, Required]
		#endif
		private AudioSourceShellCycledSound _audioSourceShellMusic;


		[SerializeField]
		#if ODIN_INSPECTOR
		[ChildGameObjectsOnly, Required]
		#endif
		private AudioSourceShellCycledSound _audioSourceShellAmbient;


		[SerializeField]
		#if ODIN_INSPECTOR
		[ChildGameObjectsOnly, Required]
		#endif
		private AudioSourceShellSound _audioSourceShellEffects;


		[SerializeField]
		#if ODIN_INSPECTOR
		[ChildGameObjectsOnly, Required]
		#endif
		private AudioSourceShellSound _audioSourceShellUI;


		public async UniTask ChangeVolumeFading(SoundCycledChannel channel, SoundSettings soundSettings, int fadeTime)
			=> await GetShell(channel).ChangeVolumeFading(soundSettings, fadeTime);
		
		public void ChangeVolumeImmediate(SoundCycledChannel channel, SoundSettings soundSettings)
			=> GetShell(channel).ChangeVolumeImmediate(soundSettings);

		public async UniTask PlayFading(SoundCycledChannel channel, AudioClip sound, SoundSettings soundSettings, FadeSettings fadeSettings)
			=> await GetShell(channel).PlayFading(sound, soundSettings, fadeSettings);


		public async UniTask StopFading(SoundCycledChannel channel, int fadeOutTime)
			=> await GetShell(channel).StopFading(fadeOutTime);


		public void PlayImmediate(SoundCycledChannel channel, AudioClip sound, SoundSettings soundSettings)
			=> GetShell(channel).PlayImmediate(sound, soundSettings);


		public void StopImmediate(SoundCycledChannel channel)
			=> GetShell(channel).StopImmediate();
		
		
		public async UniTask ChangeStereoPanFading(SoundCycledChannel channel, StereoSettings stereoSettings)
			=> await GetShell(channel).FadeStereoPan(stereoSettings);


		public void PlaySound(SoundChannel channel, AudioClip sound, float volume, float pitch)
			=> GetShell(channel).PlaySound(sound, volume, pitch);
		

		public void PlayStereoSound(SoundChannel channel, AudioClip sound, float volume, float pitch, float initialStereoPan, StereoSettings stereoSettings)
			=> GetShell(channel).PlayStereoSound(sound, volume, pitch, initialStereoPan, stereoSettings);


		public void UpdateGlobalVolume(SoundCycledChannel channel)
			=> GetShell(channel).UpdateGlobalVolume();
		
		public void UpdateGlobalVolume(SoundChannel channel)
			=> GetShell(channel).UpdateGlobalVolume();


		private AudioSourceShellCycledSound GetShell(SoundCycledChannel channel) =>
			channel switch
			{
				SoundCycledChannel.Music => _audioSourceShellMusic,
				SoundCycledChannel.Ambient => _audioSourceShellAmbient,
				_ => throw new ArgumentException("invalid enum value", nameof(channel)),
			};


		private AudioSourceShellSound GetShell(SoundChannel channel) =>
			channel switch
			{
				SoundChannel.Effects => _audioSourceShellEffects,
				SoundChannel.UI => _audioSourceShellUI,
				_ => throw new ArgumentException("invalid enum value", nameof(channel)),
			};
	}
}
