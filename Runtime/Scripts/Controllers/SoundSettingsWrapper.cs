using UnityEngine;


namespace Valhalla.SoundSystem
{
	public abstract class SoundSettingsWrapper : MonoBehaviour
	{
		public abstract bool IsMusicEnabled { get; }
		public abstract int MusicVolume { get; }
		
		public abstract bool IsSoundEnabled { get; }
		public abstract int SoundVolume { get; }


		public float GlobalMusicVolume
			=> CalculateGlobalVolume(IsMusicEnabled, MusicVolume);
		
		
		public float GlobalSoundVolume
			=> CalculateGlobalVolume(IsSoundEnabled, SoundVolume);
		
		
		private static float CalculateGlobalVolume(bool isEnabled, int settingVolume)
			=> isEnabled
				? settingVolume / 100f
				: 0;
	}
}
