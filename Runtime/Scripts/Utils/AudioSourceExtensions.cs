﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;


namespace Valhalla.SoundSystem
{
	public static class AudioSourceExtensions
	{
		private const int _MillisecondsDelay = 10;
		private const float _FloatComparisonTolerance = _MillisecondsDelay / 100000f;


		public static async UniTask FadeChangeVolume(this AudioSource audioSource, CancellationToken token, float desiredVolume = 1, int fadeTime = 1000)
		{
			audioSource.Play();
			
			if (Math.Abs(audioSource.volume - desiredVolume) < _FloatComparisonTolerance)
				return;
			
			var startVolume = audioSource.volume;
			var deltaVolume = (startVolume - desiredVolume) / (fadeTime / _MillisecondsDelay);

			for (var volume = startVolume; Math.Abs(volume - desiredVolume) > _FloatComparisonTolerance; volume -= deltaVolume)
			{
				if (token.IsCancellationRequested)
					return;

				audioSource.volume = volume;
				await UniTask.Delay(_MillisecondsDelay, DelayType.Realtime);
			}
		}


		public static async UniTask FadeInAsync(this AudioSource audioSource, CancellationToken token,
		float desiredVolume = 1, int fadeInTime = 1000)
		{
			audioSource.volume = 0;
			var deltaVolume = desiredVolume / (fadeInTime / _MillisecondsDelay);

			audioSource.Play();

			for (float volume = 0; volume <= desiredVolume; volume += deltaVolume)
			{
				if (token.IsCancellationRequested)
					return;

				audioSource.volume = volume;
				await UniTask.Delay(_MillisecondsDelay, DelayType.Realtime);
			}
		}


		public static async UniTask FadeOutAsync(this AudioSource audioSource, CancellationToken token,
		int fadeOutTime = 1000)
		{
			if (audioSource.volume == 0)
				return;

			var startVolume = audioSource.volume;
			var deltaVolume = startVolume / (fadeOutTime / _MillisecondsDelay);

			for (var volume = startVolume; volume >= 0; volume -= deltaVolume)
			{
				if (token.IsCancellationRequested)
					return;

				audioSource.volume = volume;
				await UniTask.Delay(_MillisecondsDelay, DelayType.Realtime);
			}

			audioSource.Stop();
		}


		public static async UniTask FadeStereoPan(this AudioSource audioSource, CancellationToken token, StereoSettings stereoSettings)
		{
			if (Math.Abs(audioSource.panStereo - stereoSettings.NewStereoPanValue) < _FloatComparisonTolerance)
				return;

			if (stereoSettings.FadeTime == 0)
			{
				audioSource.panStereo = stereoSettings.NewStereoPanValue;
				return;
			}

			var startStereoPan = audioSource.panStereo;
			var deltaStereoPan = (startStereoPan - stereoSettings.NewStereoPanValue) / (stereoSettings.FadeTime / _MillisecondsDelay);

			for (var stereoPan = startStereoPan; Math.Abs(stereoPan - stereoSettings.NewStereoPanValue) > _FloatComparisonTolerance; stereoPan -= deltaStereoPan)
			{
				if (token.IsCancellationRequested)
					return;

				audioSource.panStereo = stereoPan;
				await UniTask.Delay(10, DelayType.Realtime);
			}
		}


		public static void SetSettings(this AudioSource audioSource, SoundSettings soundSettings)
		{
			audioSource.loop = soundSettings.IsCycled;
			audioSource.pitch = soundSettings.Pitch;
			audioSource.volume = soundSettings.Volume;
		}
	}
}
