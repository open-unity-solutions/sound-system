## About
Multi-channel sound controller for a game.


## Usage
Scene setup:
1. Inherit and setup the `SoundSettingsWrapper`
2. Place `Sound Controller.prefab` on scene
3. Link it's children-shells to your `SoundSettingsWrapper`

Asset setup
1. Inherit `Sound` or `CycleSound`
2. Create instances and populate it with data
3. Send them to `Sound Controller`


## TODO
- [ ] Add code examples to docs
- [ ] Create example scene
- [ ] Create default setting realization
- [ ] Create default ZenJect realization
- [ ] Recreate channels with SO
